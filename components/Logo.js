import React from 'react'
import Image from 'next/image'

const Logo = props => (
    <Image src ='Tree.svg' className='logo' alt = "Logo" id = "logo_images"/>
)
export default Logo;