import {Container, Row, Col} from 'react-bootstrap'
import Image from 'next/image'

const GitLabLib = props => (
	<Container>
		<Row>
			<Col>
				<a href={`https://gitlab.com/${props.namespace}/${props.project}`}><i className="fa fa-fw fa-brands fa-gitlab"></i>Source</a>
			</Col>
			{/* Add documentation link only if the property exists */}
            <>
			{ props.documentation && <Col><a href={`${props.documentation}`}><i className="fa fa-fw fa-solid fa-book"></i>Docs</a></Col> }
            </>
			<Col>
				<a href={`https://gitlab.com/${props.namespace}/${props.project}/-/commits/${props.branch}`}><Image alt="pipeline status" src={`https://gitlab.com/${props.namespace}/${props.project}/badges/${props.branch}/pipeline.svg`}/></a>
			</Col>
			<Col>
				<a href={`https://gitlab.com/${props.namespace}/${props.project}/-/releases`}><Image alt="Latest Release" src={`https://gitlab.com/${props.namespace}/${props.project}/-/badges/release.svg`} /></a>
			</Col>
		</Row>
	</Container>
)
export default GitLabLib;