import {Container, Row, Col} from 'react-bootstrap'

const ForthSplit = props => (
	<Container fluid>
		<Row>
			<Col sm={2}>{props.lhs}</Col>
			<Col sm={8}>{props.rhs}</Col>
		</Row>
	</Container>
)
export default ForthSplit;