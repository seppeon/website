import Container from 'react-bootstrap/Container';

const Loading = () => (
    <Container className="m-5 spinner" />
)
export default Loading;