import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Link from 'next/link';
import Logo from './Logo';

const Header = props => (
  <Navbar className="bg-body-tertiary justify-content-between" bg="light" expand="lg">
    <Container>
      <Navbar.Brand as={Link} href={"/"}><Logo /></Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav>
          <Nav.Item>
            <Nav.Link as={Link} href="/">About</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link as={Link} href="/cv">CV</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link as={Link} href="/projects">Projects</Nav.Link>
          </Nav.Item>
        </Nav>
        <Nav className='ms-auto justify-content-end'>
          <Nav.Item>
            <Nav.Link as={Link} href="mailto:davidledger@live.com.au"><i className="fa fa-fw fa-envelope"></i>Email</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link as={Link} href="https://www.linkedin.com/in/david-ledger-06423b196/"><i className="fa fa-fw fa-brands fa-linkedin"></i>Linkedin</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link as={Link} href="https://www.github.com/seppeon"><i className="fa fa-fw fa-brands fa-github"></i>GitHub</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link as={Link} href="https://www.gitlab.com/seppeon"><i className="fa fa-fw fa-brands fa-gitlab"></i>GitLab</Nav.Link>
          </Nav.Item>
        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>
)
export default Header;