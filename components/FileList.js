import Link from 'next/link';
import GitLabLib from '../components/GitLabLib'

const FileList = props => (
	<>
		<GitLabLib
			namespace="seppeon"
			project="file_list"
			branch="main"
			documentation="https://seppeon.gitlab.io/file_list/file_list.html"
		/>
	</>
)
export default FileList;