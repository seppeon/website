import Link from 'next/link';
import GitLabLib from './GitLabLib'

const FileListLib = props => (
	<>
		<GitLabLib
			namespace="seppeon"
			project="file_list"
			branch="main"
			documentation="https://seppeon.gitlab.io/file_list/"
		/>
	</>
)
export default FileListLib;