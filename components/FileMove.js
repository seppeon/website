import Link from 'next/link';
import GitLabLib from '../components/GitLabLib'

const FileMove = props => (
	<>
		<GitLabLib
			namespace="seppeon"
			project="file_list"
			branch="main"
			documentation="https://seppeon.gitlab.io/file_list/file_move.html"
		/>
	</>
)
export default FileMove;