import Head   from 'next/head';
import Header from './Header';
import Footer from './Footer';
import Container from 'react-bootstrap/Container';

const Layout = props => (
    <>
    <Head>
      <title>seppeon (David Ledger)</title>
      <link rel="icon" href="/Tree.svg" sizes="any" />
      <link rel="shortcut icon" type="image/png" href="/Tree.svg"/>
      <link rel="shortcut icon" sizes="any" href="/Tree.svg"/>
      <link rel="apple-touch-icon" href="/Tree.svg"/>
    </Head>
    <Header class_name = {props.header_class} button_class = {props.header_buttons}/>
    <Container className="main">
    { props.children }
    </Container>
    <Footer class_name = {props.footer_class} button_class = {props.footer_buttons}/>
    </>
)

export default Layout;