import GitLabLib from '../components/GitLabLib'

const TL = props => (
	<>
		<GitLabLib
			namespace="seppeon"
			project="tl"
			branch="main"
			documentation="https://seppeon.gitlab.io/tl/"
		/>
	</>
)
export default TL;