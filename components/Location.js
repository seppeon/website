import LocationLink from '../components/LocationLink'

const Location = () => (
    <LocationLink long={-33.730910} lat={151.128380}>
        <i className="fa fa-3x fa-map-marker"/><br/>
        <>12/22 Eastern Rd<br/>NSW 2074<br/>AUSTRALIA</>
    </LocationLink>
)
export default Location;