import GitLabLib from '../components/GitLabLib'

const NSpan = props => (
	<>
		<GitLabLib
			namespace="seppeon"
			project="nspan"
			branch="main"
			documentation="https://seppeon.gitlab.io/nspan/"
		/>
	</>
)
export default NSpan;