import GitLabLib from '../components/GitLabLib'

const Traits = props => (
	<>
		<GitLabLib
			namespace="seppeon"
			project="traits"
			branch="main"
			documentation="https://seppeon.gitlab.io/traits/"
		/>
	</>
)
export default Traits;