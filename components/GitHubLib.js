import {Container, Row, Col} from 'react-bootstrap'
import Image from 'next/image'

const GitHubLib = props => (
	<Container>
		<Row>
			<Col><a href={`https://github.com/${props.namespace}/${props.project}`}><i className="fa fa-fw fa-brands fa-github"></i>Source</a></Col>
			<Col>
				<a href={`https://github.com/${props.namespace}/${props.project}/commits/${props.branch}`}>
					<Image alt="pipeline status" src={`https://github.com/${props.namespace}/${props.project}/actions/workflows/${props.workflow}/badge.svg`}/>
				</a>
			</Col>
			<Col><a href={`https://github.com/${props.namespace}/${props.project}/releases`}>Releases</a></Col>
		</Row>
	</Container>
)
export default GitHubLib;