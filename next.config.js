const nextConfig = {
	output: 'export',
	assetPrefix: 'https://seppeon.gitlab.io/website',
	images: {
		unoptimized: true,
	}
}

module.exports = nextConfig