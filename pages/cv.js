import React from 'react';
import { Card, CardGroup, Accordion } from 'react-bootstrap'
import ForthSplit from '../components/ForthSplit'

const Resume = (props) => (
    <>
        <h1>David Ledger</h1>
        <p>
        I enjoy snowboarding, go-karting, rock climbing, electronics, programming, and teaching. I invest my free time developing libraries and applications in C++. Recently, I have contributed to Conan(a C++ package manager), winget (a windows package manager), and the C++ standard.
        </p>
        <br/>
        <h2>Employment History­­</h2>
        <Accordion>
            <Accordion.Item eventKey="0">
                <Accordion.Header>
                    <ForthSplit lhs='Dec 2023 to Present' rhs='MTS Silicon Design Engineer @ AMD' />
                </Accordion.Header>
                <Accordion.Body>
                </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="1">
                <Accordion.Header>
                    <ForthSplit lhs='Oct 2020 to Dec 2023' rhs='Senior Firmware Design Engineer @ Rapid Response Revival' />
                </Accordion.Header>
                <Accordion.Body>
                    <ul>
                        <li>Lead development of a classifier to determine shockability of heart rhythms.</li>
                        <li>Developed firmware in C++ for a medical device.</li>
                        <li>Introduced best practices (static analysis, unit  testing, automated testing and, CI/CD) to firmware development.</li>
                        <li>Developed various desktop tools in C++ and C# for other teams to assist with automated testing.</li>
                        <li>Implement various parsers in Python, used to  enhance generated documentation for the device.</li>
                    </ul>
                </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="2">
                <Accordion.Header>
                    <ForthSplit lhs='Sept 2019 to Oct 2020' rhs='Firmware and Electronics Design Engineer @ Rapid Response Revival' />
                </Accordion.Header>
                <Accordion.Body>
                    <ul>
                        <li>Improved defibrillator frontend design which prevented  violent self-destruction.</li>
                        <li>Perform hardware and firmware testing, debug  firmware and hardware issues, develop in C++ for the STM32 microprocessor.</li>
                        <li>Authored prototype algorithm to detect heart  rhythms, collect datasets and evaluate the algorithm against the collected datasets.</li>
                    </ul>
                </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="3">
                <Accordion.Header>
                    <ForthSplit lhs='Mar 2017 to Aug 2019' rhs='Electronics Design Engineer @ EEVblog (popular engineering YouTube channel)' />
                </Accordion.Header>
                <Accordion.Body>
                    <ul>
                        <li>Assist in the design of the electronics of a Bluetooth  4.0 multimeter, development of iPhone and Android applications in C# with Xamarin for our multimeter product (121GW).</li>
                        <li>Lead development of a composite class USB device, USB-C  PD powered, isolated low noise power supply; developed firmware in C++ and CMake;  designed enclosure using OnShape.</li>
                        <li>Design and arrange mass production of a parts  storage solution.</li>
                        <li>Develop various applications to automate testing of  products in C# and WinForms, through interfacing with instruments.</li>
                    </ul>
                </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="4">
                <Accordion.Header>
                    <ForthSplit lhs='Mar 2013 to Dec 2017' rhs='Junior Engineer and Teaching Assistant @  University of Technology, Sydney' />
                </Accordion.Header>
                <Accordion.Body>
                    <ul>
                        <li>Lead development of an educational kit with five  modules, their supporting documentation and firmware libraries for student engineers (Electrical &amp; Mechatronics). This included electronics design, PCB layout  and coordinated manufacturing of the modules. Hundreds of students used the kit  successfully.</li>
                        <li>Designed in SolidWorks, a batch of an enclosures for  the power electronics department.</li>
                        <li>Lead development of an energy harvesting data-logger which was an outreach project for high school students. I developed a desktop application in C#, and the firmware in C.</li>
                        <li>Lead development of the “Ball and Beam” project,  used by control theory students. This involved mechanical, electronics design and  C firmware design.</li>
                        <li>Teaching assistant for subjects such as “Electronics and Circuits” and “Embedded Software”.</li>
                        <li>Run soldering classes for hundreds of students each semester(there were 2 a year).</li>
                    </ul>
                </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="5">
                <Accordion.Header>
                    <ForthSplit lhs='Jan 2016 to Mar 2016' rhs='Electronics Engineer @ Quberider' />
                </Accordion.Header>
                <Accordion.Body>
                    <ul>
                        <li>Develop, teach, and document a Python library that went in a product sent to the  international space station (ASIMOV CubeSat).</li>
                    </ul>
                </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="6">
                <Accordion.Header>
                    <ForthSplit lhs='Feb 2015 to Apr 2017' rhs='Junior Engineer @ EEVBlog' />
                </Accordion.Header>
                <Accordion.Body>
                    <ul>
                        <li>PCB Layouts, schematic design, mechanical design,  fabricate various prototypes.</li>
                    </ul>
                </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="7">
                <Accordion.Header>
                    <ForthSplit lhs='Jun 2012 to Jan 2013' rhs='Student Engineer @ Ingenuity Electronics Design' />
                </Accordion.Header>
                <Accordion.Body>
                    <ul>
                        <li>Layout  PCB, develop firmware in C and do EMC testing for two different water saving  products.</li>
                        <li>Work with other engineers to design and build a  production test jig.</li>
                        <li>Assist on development of the KeyFinder, a Bluetooth LE product.</li>
                        <li>Perform soldering and debugging for various  products, assisting other engineers with their projects.</li>
                        <li>Perform graphics design for the website, add a  carousel to the website and highlight their projects.</li>
                        <li>Develop the mechanical design for the self-monitoring  mining idler prototype.</li>
                    </ul>
                </Accordion.Body>
            </Accordion.Item>
        </Accordion>
        <br/><h2>Education</h2>
        <ul>
            <li>
                <strong>2011 to 2017: Bachelor of Engineering  (Electrical) + First Class Honors</strong>
                <br />
                University of Technology, Sydney
            </li>
            <li>
                <strong>2017: Chapman Award for Innovation</strong>
                <br />
                University of Technology, Sydney
            </li>
            <li>
                <strong>2016: Altium Essentials Training</strong>
                <br />
                Altium
            </li>
        </ul>
        <br/><h2>Skills</h2>
        <CardGroup>
            <Card>
                <Card.Body>
                    <Card.Title>Microcontrollers/FPGA</Card.Title>
                    <Card.Text>STM32L, STM32F, NRF52, PIC32, PIC18, PIC16, TIVA/Stellaris, TMS320, MSP430, LPC18 and Spartan 6 FPGA.</Card.Text>
                </Card.Body>
            </Card>
            <Card>
                <Card.Body>
                    <Card.Title>Electronics Design</Card.Title>
                    <Card.Text>Altium Designer, LTSpice, Falstad and, OrCAD.</Card.Text>
                </Card.Body>
            </Card>
            <Card>
                <Card.Body>
                    <Card.Title>3d Modelling</Card.Title>
                    <Card.Text>SolidWorks and Onshape.</Card.Text>
                </Card.Body>
            </Card>
            <Card>
                <Card.Body>
                    <Card.Title>Regulatory</Card.Title>
                    <Card.Text>Medical device standards: IEC 62304, IEC 13485, IEC 60601, IEC 80002; EMC testing experience; Coauthor of P2738R1 approved for inclusion in ISO C++26 Standard and; Public reviewer of MISRA C++ standard.</Card.Text>
                </Card.Body>
            </Card>
        </CardGroup>
    </>
)
export default Resume;