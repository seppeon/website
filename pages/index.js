import React from 'react';

const Home = () => (
  <>
  This is the website of David Ledger, online I usually go by <b>seppeon</b>!
  <br/><br/>
  I enjoy snowboarding, go-karting, rock climbing, electronics, programming, and teaching. I invest my free time developing open-source    libraries in C++. Recently, I have contributed to Conan(a C++ package manager), winget (a windows package manager), and the C++ standard.
  </>
);
export default Home;