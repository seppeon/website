import React from 'react';
import Layout from '../components/Layout';
import App from 'next/app';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/global.css';
import '../node_modules/font-awesome/css/font-awesome.min.css'

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props
    return (
      <Layout>
        <Component {...pageProps} />
      </Layout>)
  }
}

export default MyApp