import React from 'react';
import { Accordion } from 'react-bootstrap'
import Image from 'next/image'
import Link from 'next/link'
import GitLabLib from '../components/GitLabLib'
import GitHubLib from '../components/GitHubLib';
import ForthSplit from '../components/ForthSplit'
import FileListLib from '../components/FileListLib'
import FileList from '../components/FileList'
import FileMove from '../components/FileMove'
import FileResolve from '../components/FileResolve'
import NSpan from '../components/NSpan'
import TL from '../components/TL';
import Traits from '../components/Traits';

const Projects = (props) => (
	<>
	<p>This page is under construction and is incomplete.</p>

	<h1>Projects</h1>
	<h2>C++ Libraries</h2>
	<Accordion alwaysOpen>
		<Accordion.Item eventKey="0">
			<Accordion.Header>
				<ForthSplit lhs="Mm" rhs="C++ Geometric Algebra Library."/>
			</Accordion.Header>
			<Accordion.Body>
				<GitLabLib
					namespace="seppeon"
					project="Mm"
					branch="main"
				/>
			</Accordion.Body>
		</Accordion.Item>
		<Accordion.Item eventKey="1">
			<Accordion.Header>
				<ForthSplit lhs="SCoro" rhs="A stackless, resumable, resettable state machine style coroutine library for C++17."/>
			</Accordion.Header>
			<Accordion.Body>
				<GitHubLib
					namespace="seppeon"
					project="scoro"
					branch="master"
					workflow="cmake.yml"
				/>
			</Accordion.Body>
		</Accordion.Item>
		<Accordion.Item eventKey="2">
			<Accordion.Header>
				<ForthSplit lhs="FnRef" rhs="A library that provides a type-erased, non-owning reference to a function."/>
			</Accordion.Header>
			<Accordion.Body>
				<GitLabLib
					namespace="seppeon"
					project="FnRef"
					branch="main"
				/>
			</Accordion.Body>
		</Accordion.Item>
		<Accordion.Item eventKey="3">
			<Accordion.Header>
				<ForthSplit lhs="OpCpo" rhs="Library that puts all built in operators into named CPOs."/>
			</Accordion.Header>
			<Accordion.Body>
				<GitLabLib
					namespace="seppeon"
					project="libOpCpo"
					branch="main"
				/>
			</Accordion.Body>
		</Accordion.Item>
		<Accordion.Item eventKey="4">
			<Accordion.Header>
				<ForthSplit lhs="FL" rhs="A library providing cross platform support for renaming, finding and resolving files (as per a user defined set of rules)."/>
			</Accordion.Header>
			<Accordion.Body>
				<FileListLib/>
			</Accordion.Body>
		</Accordion.Item>
		<Accordion.Item eventKey="5">
			<Accordion.Header>
				<ForthSplit lhs="NSpan" rhs="Provides a view of multiple contiguous sets of data."/>
			</Accordion.Header>
			<Accordion.Body>
				<NSpan/>
			</Accordion.Body>
		</Accordion.Item>
		<Accordion.Item eventKey="6">
			<Accordion.Header>
				<ForthSplit lhs="TL" rhs="A library to manipulate lists of template parameters."/>
			</Accordion.Header>
			<Accordion.Body>
				<TL/>
			</Accordion.Body>
		</Accordion.Item>
		<Accordion.Item eventKey="7">
			<Accordion.Header>
				<ForthSplit lhs="Traits" rhs="A library of type-traits. Modify and query properties on types, this includes a function traits library and a set of C++20 concepts."/>
			</Accordion.Header>
			<Accordion.Body>
				<Traits/>
			</Accordion.Body>
		</Accordion.Item>
	</Accordion>

	<br/><h2>Guides</h2>
	<Accordion alwaysOpen>
		<Accordion.Item eventKey="0">
			<Accordion.Header>
				<ForthSplit lhs="Modern GCC on windows" rhs="A step-by-step guide to setting up a windows machine with a GCC environment for C++ development."/>
			</Accordion.Header>
			<Accordion.Body>
				<GitLabLib
					namespace="seppeon"
					project="cpp-project-setup"
					branch="master"
				/>
			</Accordion.Body>
		</Accordion.Item>
	</Accordion>

	<br/><h2>Applications</h2>
	<Accordion alwaysOpen>
		<Accordion.Item eventKey="0">
			<Accordion.Header>
				<ForthSplit lhs="FileList" rhs="An application to search for files, by name with glob search patterns."/>
			</Accordion.Header>
			<Accordion.Body>
				<FileList/>
			</Accordion.Body>
		</Accordion.Item>
		<Accordion.Item eventKey="1">
			<Accordion.Header>
				<ForthSplit lhs="FileMove" rhs="An application to move files, with destinations dependant on capture groups from the input path."/>
			</Accordion.Header>
			<Accordion.Body>
				<FileMove/>
			</Accordion.Body>
		</Accordion.Item>
		<Accordion.Item eventKey="2">
			<Accordion.Header>
				<ForthSplit lhs="FileResolve" rhs="An application to resolve the location of files (DLLs, executables, header files etc...), using a set of user defined rules. For example, when mimicking the behavior of a compiler to resolve the location of some header."/>
			</Accordion.Header>
			<Accordion.Body>
				<FileResolve/>
			</Accordion.Body>
		</Accordion.Item>
	</Accordion>

	<br/><h2>Websites</h2>
	<Accordion alwaysOpen>
		<Accordion.Item eventKey="0">
			<Accordion.Header>
				<ForthSplit lhs=<Link href="https://engsil.com">engsil.com</Link> rhs="The node.js project for my company's website"/>
			</Accordion.Header>
			<Accordion.Body>
				<GitLabLib
					namespace="engsil"
					project="website"
					branch="main"
				/>
			</Accordion.Body>
		</Accordion.Item>
		<Accordion.Item eventKey="1">
			<Accordion.Header>
				<ForthSplit lhs=<Link href="https://seppeon.com">seppeon.com</Link> rhs="The node.js project for seppeon.com"/>
			</Accordion.Header>
			<Accordion.Body>
				<GitLabLib
					namespace="seppeon"
					project="website"
					branch="main"
				/>
			</Accordion.Body>
		</Accordion.Item>
	</Accordion>
	</>
);
export default Projects;